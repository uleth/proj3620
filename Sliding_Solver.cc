/**
@file Board_Tile.cc
@brief 
@author Josh Moan
@author Felix Schiel
@author Dawson Meyer
*/


#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <cmath>
#include "Sliding_Solver.h"



Sliding_Solver::Sliding_Solver( std::string& s, std::string& g): startconfig(s), goalconfig(g)
{
   // string goal("123705486");
   //Solve_Puzzle(s, goal);
   cout<<"constructed?";
}




void Sliding_Solver::Solve_Puzzle(Sliding_Solver s)
{
   cout<<"solve puzzle started";
   string current=s.startconfig;
   string goal=s.goalconfig;
   // Board_Tile* cboard;
   Board_Tile* cboard(s.startconfig);
   cout<<"just before whileloop" ;
   std::list<Board_Tile>* temp;
   
    while(current!=goal)
    {
       // cboard= new Board_Tile(current);
       cboard->printBoard();
      //(*cboard).printBoard();
      // tempPriority= new  std::priority_queue //this is also bad
      // <pair<int, Board_Tile>,vector<pair<int, Board_Tile>>, comparator>;
      //cout<<"just before temp";
      temp= new std::list<Board_Tile>;
      
      (*temp)=(*cboard).nextConfigs( ( *cboard ) );
      
      //std::list<Board_Tile>:: iterator it=temp.begin();
      //(*it).printBoard();
      // (*tempPriority).push(std::make_pair(((*it).Manhattan_Distance(goal)+(*it).numMoves()), (*it)));
      // cout<<"pushed once";
      //for(std::list<Board_Tile>::iterator it=temp.begin(); it!= temp.end(); ++it)
      //{//sort next configurations by Manhat+numMoves
      // cout<<"inside for loop";
      //segfaults here
      // (*tempPriority).push(std::make_pair(((*it).Manhattan_Distance(goal)+(*it).numMoves()),(*it)));
      // cout<<"loop did once ";
      // }
      if(!(allconfigs.empty())){
	 for(std::list<Board_Tile>::iterator it=allconfigs.begin();it!=allconfigs.end(); ++it)
	 {
	    //std::list<Board_Tile>::iterator it2=temp.begin();
	    if((*temp).front()==(*it))
	    {
	       if((*temp).size()>1)
	       {
		  (*temp).pop_front();
		  it=allconfigs.begin();
	       }
	       else break;
	    }
	 }
      }
      allconfigs.push_back((*temp).front());
      priorityboard.push(std::make_pair( (Adist((*temp).front())), (*temp).front()));
 
      current=priorityboard.top().second.config;

      // goal=current;
      
      delete cboard;
      delete temp;
    }
    std::cout<<priorityboard.top().second.moves_from_start;
}
int Sliding_Solver::Adist(Board_Tile& B){
   int adist=0;
   adist+=B.Manhattan_Distance(goalconfig);
   adist+=B.numMoves();
   
   return adist;
}
	 

	    

	    
//inserts elements into priority queue
   //priorityboard.push(std::make_pair(((*it).Manhattan_Distance(goal)+(*it).numMoves()),(*it)));
