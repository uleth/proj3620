CC= g++11
CFLAGS= -Wall -g -ldl -std=c++11 -lpthread -lX11
OBJS = main.o Board_Tile.o Sliding_Solver.o

main: $(OBJS)
	$(CC) -o $@ $(OBJS)

#Board_Tile.o: Board_Tile.h
%.o : %.cc
	$(CC) $(CFLAGS) -c $^

.PHONY : clean
clean:
	rm -rf *.o *~ *.d main
